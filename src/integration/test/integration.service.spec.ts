import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from 'src/app.module';
import integrationConfig from '../config/integration.config';
import { IntegrationService } from '../integration.service';
import { HackerNewsRepository } from '../repository/news-repository';
import { HackerNew } from '../utils/news.response';

describe('IntegrationService', () => {
  let integrationService: IntegrationService;
  let news: HackerNew[];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          load: [integrationConfig],
        }),
        ScheduleModule.forRoot(),
        HttpModule,
        AppModule,
      ],
      providers: [IntegrationService, HackerNewsRepository],
    }).compile();

    integrationService = module.get<IntegrationService>(IntegrationService);
    news = await integrationService.getHackerNews();
  });

  it('Should be return a response', async () => {
    expect(news.length).toBeGreaterThan(0);
  });

  it('Every element should be a instance of NewsResponse', async () => {
    news.forEach((n) => expect(n).toBeInstanceOf(HackerNew));
    console.log(news[0]);
  });

  it('Should populate database correctly', async () => {
    await integrationService.initialize();
  });
});
