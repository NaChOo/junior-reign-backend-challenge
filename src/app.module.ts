import { Module } from '@nestjs/common';
import { NewsModule } from './news/news.module';
import { ConfigModule } from '@nestjs/config';
import appConfig from './config/app.config';
import { IntegrationModule } from './integration/integration.module';
import { PrismaModule } from './prisma/prisma.module';
@Module({
  imports: [
    ConfigModule.forRoot({
      load: [appConfig],
    }),
    NewsModule,
    IntegrationModule,
    PrismaModule,
  ],
})
export class AppModule {}
