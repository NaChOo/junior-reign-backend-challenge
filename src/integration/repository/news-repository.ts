import { HttpService } from '@nestjs/axios';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { firstValueFrom } from 'rxjs';
import integrationConfig from '../config/integration.config';
import { AxiosResponse } from 'axios';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class HackerNewsRepository {
  constructor(
    @Inject(integrationConfig.KEY)
    private readonly integrationConfiguration: ConfigType<
      typeof integrationConfig
    >,
    private readonly prisma: PrismaService,
    private httpService: HttpService,
  ) {}
  async fetchNewsFromExternalSource(): Promise<AxiosResponse> {
    return firstValueFrom(
      this.httpService.get(
        `${this.integrationConfiguration.URL}?&hitsPerPage=${this.integrationConfiguration.MAX_NEWS}`,
      ),
    );
  }
}
