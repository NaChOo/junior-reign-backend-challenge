import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsPositive } from 'class-validator';

export class DeleteNewDto {
  @ApiProperty({
    description: "The New's id to remove.",
  })
  @IsDefined()
  @IsPositive()
  readonly id: number;
}
