import { Injectable } from '@nestjs/common';
import { New } from '@prisma/client';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';

import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class NewsRepository {
  constructor(private readonly prismaService: PrismaService) {}

  getNewsPaginated(
    paginationQuery: PaginationQueryDto,
    where: Record<any, any>,
  ): Promise<any> {
    const { newsPerPage = 5, page = 1 } = paginationQuery;
    return this.prismaService.new.findMany({
      skip: newsPerPage * (page - 1),
      take: newsPerPage,
      where,
      select: {
        id: true,
        title: true,
        story: true,
        comment: true,
        tags: {
          select: { name: true },
        },
        Author: {
          select: { name: true },
        },
      },
    });
  }

  countNews(where: Record<any, any>): Promise<number | undefined> {
    return this.prismaService.new.count({
      where: { ...where },
    });
  }

  getNewById(id: number): Promise<New | undefined> {
    return this.prismaService.new.findFirst({
      where: { id, disabled: false },
    });
  }

  disableNewById(id: number): Promise<New | undefined> {
    return this.prismaService.new.update({
      data: { disabled: true },
      where: { id },
    });
  }
}
