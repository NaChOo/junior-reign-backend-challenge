import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsPositive, Length, Min } from 'class-validator';

export class PaginationQueryDto {
  @ApiProperty({
    description: 'Number of news to show per page',
    default: 5,
  })
  @IsPositive()
  @IsOptional()
  @Min(1)
  readonly newsPerPage?: number;

  @ApiProperty({
    description: 'The page to lookup.',
    default: 1,
  })
  @IsOptional()
  @IsPositive()
  @Min(1)
  readonly page?: number;

  @ApiProperty({
    description: 'Filter the news by author.',
  })
  @IsOptional()
  @Length(0, 100)
  readonly author?: string;

  @ApiProperty({
    description: 'Filter news by tag.',
  })
  @IsOptional()
  @Length(0, 100)
  readonly tag?: string;

  @ApiProperty({
    description: 'Filter news by title.',
  })
  @IsOptional()
  @Length(0, 100)
  readonly title?: string;
}
