import { Injectable, Logger } from '@nestjs/common';
import { HackerNewsRepository } from './repository/news-repository';
import { AxiosResponse } from 'axios';
import { HackerNew } from './utils/news.response';
import { plainToInstance } from 'class-transformer';
import { PrismaService } from 'src/prisma/prisma.service';
import { PrismaClient } from '@prisma/client';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class IntegrationService {
  private readonly logger = new Logger(IntegrationService.name);
  constructor(
    private readonly newsRepository: HackerNewsRepository,
    private readonly prismaService: PrismaService,
  ) {}

  @Cron(CronExpression.EVERY_10_HOURS)
  async initialize(): Promise<void> {
    this.logger.log('Integration started.');
    const hackerNews: HackerNew[] = await this.getHackerNews();
    await this.mergeNews(hackerNews);
    this.logger.log('Integration finished.');
  }

  async getHackerNews(): Promise<HackerNew[]> {
    const response: AxiosResponse =
      await this.newsRepository.fetchNewsFromExternalSource();

    const hitsArray: Record<any, any>[] = response.data.hits;
    const hackerNews: HackerNew[] = plainToInstance(HackerNew, hitsArray);

    this.logger.log(`Total HR News recovered: ${hackerNews.length}`);
    return hackerNews;
  }

  mergeNews(hackerNews: HackerNew[]): Promise<void> {
    return this.prismaService.$transaction(
      async (transaction: PrismaClient) => {
        await Promise.all(
          hackerNews.map((hackerNew: HackerNew) => {
            return this.checkIfNewHaveToBeInserted(hackerNew, transaction);
          }),
        );
      },
    );
  }

  async checkIfNewHaveToBeInserted(
    hackerNew: HackerNew,
    transaction: PrismaClient,
  ): Promise<void> {
    const externalId: number = hackerNew.externalId;

    const existingNew = await transaction.new.findUnique({
      where: { externalId },
    });

    if (!existingNew) {
      const tagsToInsert: Array<any> = hackerNew.tags.map((tagName) => {
        return {
          where: { name: tagName },
          create: { name: tagName },
        };
      });
      await transaction.new.create({
        data: {
          ...hackerNew.newData,
          Author: {
            connectOrCreate: {
              where: { name: hackerNew.author },
              create: { name: hackerNew.author },
            },
          },
          tags: {
            connectOrCreate: tagsToInsert,
          },
        },
      });
    }
  }
}
