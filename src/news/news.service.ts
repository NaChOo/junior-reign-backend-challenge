import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { NewsRepository } from './news.repository';

@Injectable()
export class NewsService {
  constructor(private readonly newsRepository: NewsRepository) {}

  async findAll(paginationQuery: PaginationQueryDto) {
    const { newsPerPage = 5, author, tag, title } = paginationQuery;

    const newsWhere = {
      disabled: false,
      title,
      Author: {
        name: { contains: author },
      },
      tags: {
        some: { name: { contains: tag } },
      },
    };

    const totalPages = Math.ceil(
      (await this.newsRepository.countNews(newsWhere)) / newsPerPage,
    );

    const news = await this.newsRepository.getNewsPaginated(
      paginationQuery,
      newsWhere,
    );

    return {
      totalPages,
      news,
    };
  }

  async disableNew(id: number): Promise<void> {
    const newExist = await this.newsRepository.getNewById(id);
    if (!newExist) throw new NotFoundException(`New #${id} not found`);

    await this.newsRepository.disableNewById(id);
  }
}
