import { Controller, Delete, Get, Param, Query } from '@nestjs/common';
import { ApiNotFoundResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { DeleteNewDto } from 'src/common/dto/delete-new.dto';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { NewsService } from './news.service';
@ApiTags('news')
@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @ApiResponse({ status: 200, description: 'All news listed successfully.' })
  @Get()
  findAll(@Query() paginationQuery: PaginationQueryDto) {
    return this.newsService.findAll(paginationQuery);
  }

  @ApiNotFoundResponse({ description: 'No new found by id.' })
  @ApiResponse({ status: 200, description: 'New deleted successfully.' })
  @Delete(':id')
  delete(@Param() newToDelete: DeleteNewDto) {
    const { id } = newToDelete;
    return this.newsService.disableNew(id);
  }
}
