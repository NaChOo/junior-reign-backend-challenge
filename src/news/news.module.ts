import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/prisma/prisma.module';
import { NewsController } from './news.controller';
import { NewsRepository } from './news.repository';
import { NewsService } from './news.service';

@Module({
  imports: [PrismaModule],
  providers: [NewsService, NewsRepository],
  controllers: [NewsController],
})
export class NewsModule {}
