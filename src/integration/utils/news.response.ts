import { Exclude, Expose, Type } from 'class-transformer';

@Exclude()
export class HackerNew {
  @Expose({ name: 'objectID' })
  @Type(() => Number)
  private readonly _externalId: number;
  @Expose({ name: 'title' })
  private readonly _title: string;
  @Expose({ name: 'story_title' })
  private readonly _story_title: string;
  @Expose({ name: 'story_text' })
  private readonly _story: string;
  @Expose({ name: 'comment_text' })
  private readonly _comment: string;
  @Expose({ name: 'author' })
  private readonly _author: string;
  @Expose({ name: '_tags' })
  private readonly _tags: string[];

  public get newData() {
    return {
      externalId: this._externalId,
      title: this._title ?? this._story_title,
      story: this._story,
      comment: this._comment,
    };
  }
  public get author(): string {
    return this._author;
  }
  public get tags(): Array<string> {
    return this._tags;
  }
  public get externalId(): number {
    return this._externalId;
  }
}
