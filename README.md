## Initial description

This is the response for the challenge sent by reign for the junior backend developer position.

This test want two technical functionalities finished:

- Fetch news form the api and save it locally on the database. (Every hour)
- List all news from local database, paginated and filter by:
  - Author
  - Tag
  - Title

This was the initial model for the database, the HR news has a story and comment at the same api level.

  <div style="display: flex; justify-content: center; margin-top: 16px; margin-bottom: 32px; ">
  <img src="./images/ideal.jpg"
     alt="Markdown Monster icon"
     style="display: block; float: left; width: 300px" />
  </div>

However, the challenge requirement may be not adjust for the first model created, because for example, all of the tags indicates the type(comment or story), the author, and the storyId(unique ObjectID), and with that may be irrelevant filter by "tag". So to make de challenge requirement relevant, the model used is the next to at least filter by some tags and the author:

  <div style="display: flex; justify-content: center; margin-top: 16px; margin-bottom: 32px; ">
  <img src="./images/final.jpg"
     alt="Markdown Monster icon"
     style="display: block; float: left; width: 300px" />
  </div>

### Many things to improve:

- Implement good unit testing and end2end testing.
  - Use prisma mocking for Unit testing [PrismaMock](https://www.prisma.io/docs/guides/testing/unit-testing)
- Refactor the code and create NewsRepository on NewsModule for better testing and CI.
- Good configuration of the GitLab CI/CD, there is a basic CI running for the lint.
  - Use a QA database to run test, like free heroku PostgreSQL plugin.
- Run the integration by command (not finished), [Look at](https://gitlab.com/NaChOo/junior-reign-backend-challenge/-/blob/master/src/console.ts)
- And many thinks.

https://gitlab.com/NaChOo/junior-reign-backend-challenge/-/blob/master/src/console.ts

## Step to run locally

## 1 Create database

First we need to initialize the database for the project. There is a docker-compose file to run on the docker folder.

```bash
$ docker-compose up
```

## 2 Connect to database

With the database already created. Now we have to set the .env variables to connect to the database and the API endpoint to retrieve the data. Use the .env.example with this recommended configuration.

```bash
DATABASE_USER=postgres
DATABASE_PASSWORD=postgres
DATABASE_NAME=hacker-news
DATABASE_PORT=5432
DATABASE_HOST=localhost

DATABASE_URL="postgresql://postgres:postgres@127.0.0.1/hacker-news?statusColor=686B6F&enviroment=local&name=HACKER-NEWS&tLSMode=0&usePrivateKey=false&safeModeLevel=0&advancedSafeModeLevel=0&driverVersion=0"

HACKER_NEWS_URL="https://hn.algolia.com/api/v1/search_by_date?query=nodejs"
MAX_NEWS=20
```

## 3 Dependencies and migration

With the database already created and the .env. THe migrations have to run to create the tables before populate the database with real data, but before that we have to install the project dependencies and then run de migration.

```bash
# Install project dependencies
$ npm i
# Run the migrations to populate the database
$ npx prisma migrate dev
# Generate the prisma client for the connection
$ npx prisma generate
```

## 4 Running the app

```bash
# watch mode
$ npm run start:dev
```

## 5 Populate the database.

To avoid wait an hour to populate the database. Modify the cron interval on the IntegrationService and set to EVERY_10_SECONDS
`EVERY_10_SECONDS`.

```js
  @Cron(CronExpression.EVERY_HOUR) // <= CronExpression.EVERY_10_SECONDS
  async initialize(): Promise<void> {
    this.logger.log('Integration started.');
```

The exported postman file is on the root folder called `postman` [here](https://gitlab.com/NaChOo/junior-reign-backend-challenge/-/tree/master/postman).

When the project runs, a swagger documentation is generated at the route `/api` on the browser.
