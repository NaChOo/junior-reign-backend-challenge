import { registerAs } from '@nestjs/config';

export default registerAs('integration', () => ({
  URL: process.env.HACKER_NEWS_URL,
  MAX_NEWS: parseInt(process.env.MAX_NEWS, 10) || 20,
}));
