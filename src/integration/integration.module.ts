import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { PrismaModule } from 'src/prisma/prisma.module';
import integrationConfig from './config/integration.config';
import { IntegrationService } from './integration.service';
import { HackerNewsRepository } from './repository/news-repository';

@Module({
  imports: [
    ConfigModule.forFeature(integrationConfig),
    HttpModule,
    ScheduleModule.forRoot(),
    PrismaModule,
  ],
  providers: [IntegrationService, HackerNewsRepository],
})
export class IntegrationModule {}
