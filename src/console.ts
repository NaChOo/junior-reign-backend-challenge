import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { IntegrationModule } from './integration/integration.module';
import { IntegrationService } from './integration/integration.service';

async function bootstrap() {
  const application = await NestFactory.createApplicationContext(AppModule);

  const command = process.argv[2];

  switch (command) {
    case 'run-integration':
      console.log('run-integration');
      const integrationService = application
        .select(IntegrationModule)
        .get(IntegrationService);
      await integrationService.initialize();
      break;
    default:
      console.log('Command not found');
      process.exit(1);
  }

  await application.close();
  process.exit(0);
}

bootstrap();
